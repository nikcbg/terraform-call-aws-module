//--------------------------------------------------------------------
// Variables
variable "ec2_module_access_key" {}
variable "ec2_module_ami" {}
variable "ec2_module_instance_type" {}
variable "ec2_module_region" {}
variable "ec2_module_secret_key" {}

//--------------------------------------------------------------------
// Modules
module "ec2_module" {
  source  = "app.terraform.io/tforg123/ec2-module/aws"
  version = "0.1"

  access_key = "${var.ec2_module_access_key}"
  ami = "${var.ec2_module_ami}"
  instance_type = "${var.ec2_module_instance_type}"
  region = "${var.ec2_module_region}"
  secret_key = "${var.ec2_module_secret_key}"
}